## This is now archived, it should still work, but in all honesty, it was poorly written from the beginning, and I see no reason to continue maintaining it.

# Discord posting bot for Minecraft
This is a bash script that will use nmap to scan a Minecraft server and detect when there is a change in players. If there is a change in players, it will post to discord using a webhooks URL
# Requirements
Bash (Included with most Debain-based distributions), GNU screen, can be installed with sudo apt-get install screen (on Debian based), cURL (Included in most Debian based Distributions)
# How to use
1. [Get a webhooks URL from Discord for a channel](https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks) 
2. Download the script, You will need to modify some parts at the top of the script, such as webHooksURL='' in order to get it working.
3. After replacing all the options in the script and insuring you have all the requirements installed go ahead and run it.

# Just want to Tweet the number of players? [See this link](https://gitlab.com/Really-Random-User/mc-server-twitter-bot/)