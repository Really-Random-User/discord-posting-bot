#!/bin/bash
#This script requires the following packages to work
#screen, bash, nmap
##CONFIGURATION, defaults only work for one local MC server running on port 25565
name='count-bot'
#The username that shows up when posting to discord using the webhook
webHookURL=''
#This is the address to connect to server ex. hypixel.net
address=localhost
#port number for the minecraft server
port=25565
#name of screen session to create (for using curl (solution to complicated bash quoting)) use that if you are planning to set up more than one server monitor on one device
#DO NOT USE SPACES
screenname=curlscreen
#Text before number of players and time statement
textbefore="There are"
#Script uses nmap to detect if players are online for a minecraft server
##DO NOT EDIT BEYOND THIS POINT UNLESS YOU KNOW WHAT YOU ARE DOING
sleep 20
isrstreamup=$( screen -list | grep -o $screenname )
if [ "$isrstreamup" = $screenname ]
then
	echo screen session already running!!
else
	screen -dmS $screenname
	sleep 2
	run=yes
	compareVar=notsetyet
fi
while [ "$run" = "yes" ]
do
	anyoneOn=$( nmap -A -p $port $address | grep -o "\<[0-9]*\/[0-9]*" | grep -v "$port" )
	sleep 10
	if [ "$anyoneOn" = "$compareVar" ]
	then
		date=$( date "+%F-%T" )
		echo $date no change in players and therefore not posting to discord press ctrl+c to quit
		sleep 20
	else
		    compareVar=$anyoneOn
			screen -S $screenname -X stuff 'curl -H "Content-Type: application/json" -X POST -d '
			screen -S $screenname -X stuff "'"
			screen -S $screenname -X stuff '{"username":'
			screen -S $screenname -X stuff ' "'
			screen -S $screenname -X stuff "$name"
			screen -S $screenname -X stuff '"'
			screen -S $screenname -X stuff ', "'
			screen -S $screenname -X stuff 'content": '
			screen -S $screenname -X stuff '"'
			#Below is where the text CONTENT of the Discord post is entered
			screen -S $screenname -X stuff "$textbefore $compareVar players online"
			screen -S $screenname -X stuff '"'
			screen -S $screenname -X stuff '}'
			screen -S $screenname -X stuff "'"
			screen -S $screenname -X stuff ' '
			screen -S $screenname -X stuff "$webHookURL"
			screen -S $screenname -X stuff " \n"
		fi
done
#Command to post to webhook is curl -H "Content-Type: application/json" -X POST -d '{"username": "test", "content": "hello"}' <webhook url>